import binascii
import os

from django.conf import settings
from django.contrib.auth.models import BaseUserManager
from django.core import signing
from django.db import models
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from ..base.utils import timezone


def _generate_code():
    return binascii.hexlify(os.urandom(20))

class UserManager(BaseUserManager):

    def _create_user(self, email, password,role,gender,
                     is_staff, is_active,  is_superuser, **extra_fields):
   
        
        if not email:
            raise ValueError('Please enter email')
        email = self.normalize_email(email)
        user = self.model(email=email,role=role,gender=gender,
                          is_staff=is_staff, is_active=is_active,
                          is_superuser=is_superuser, **extra_fields)
        user.set_password(password)

        user.save(using=self._db)
        return user

    def create_user(self, email,role,gender, password=None, **extra_fields):
        is_active = extra_fields.pop("is_active", False)
        return self._create_user(email, password,role,gender, False, is_active, False,
                                 **extra_fields)


    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True, True,
                                 **extra_fields)


class PasswordResetCodeManager(models.Manager):

    def create_reset_code(self, user):

        uid = urlsafe_base64_encode(force_bytes(user.pk))
        code = _generate_code()
        password_reset_code = self.create(user=user, code=code, uid=uid)
        return password_reset_code



