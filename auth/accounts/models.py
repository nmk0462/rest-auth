from typing import Generator
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser,PermissionsMixin
from django.db.models.enums import Choices
from rest_framework.authtoken.models import Token
from .managers import UserManager,PasswordResetCodeManager
from django.db import models
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
import logging
logger = logging.getLogger(__name__)
class User(AbstractBaseUser,PermissionsMixin):
    Gender_choice=(
        ('M' , 'Male'),
        ('F' , 'Female')

    )
    mobile = models.CharField(max_length=128, blank=True, null=True, default='', unique=True)
    email = models.EmailField(max_length=255, null=True, blank=True, default='', unique=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    role=models.CharField(max_length=64,default="general")
    gender=models.CharField(max_length=10,choices=Gender_choice,default='M')
    
    objects = UserManager()

    USERNAME_FIELD = 'email'

class AbstractBaseCode(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="User_Abstract", on_delete=models.PROTECT)
    code = models.CharField(_('code'), max_length=255, primary_key=True)
    uid = models.CharField(max_length=40, default='uidrequired')


    class Meta:
        abstract = True

    def send_email(self):
        subject = "Reset Your Password"
        text_content=(str(self.uid) +"/" + str(self.code))
           
        from_email = settings.EMAIL_HOST_USER
        to_mail = self.user.email

        try:
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to_mail])

            msg.send()
            print("Email Sent succesfully")
        except Exception:
            logger.exception("Unable to send the mail.")

    



class PasswordResetCode(AbstractBaseCode):
    objects = PasswordResetCodeManager()

    def send_password_reset_email(self):
        self.send_email()
    


