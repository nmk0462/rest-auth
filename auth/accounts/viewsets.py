from django.db.models import query,Count
from rest_framework.serializers import Serializer
from .models import  User,PasswordResetCode
from .serializers import UserSerializer,PasswordResetSerializer
from .services import auth_register_user,auth_login,auth_password_change, _parse_data,get_user_from_email_or_mobile
from ..base import response
from django.conf import settings
from django.contrib.auth import logout, get_user_model
from django.core import signing
from rest_framework.authtoken.models import Token
from rest_framework.decorators import action, permission_classes
from django.contrib.auth import logout, get_user_model
from rest_framework import viewsets
from rest_framework.authtoken.models import Token
from django.contrib.auth.decorators import login_required
import logging
from .permissions import accessPermission,accessPermission1
from functools import partial
from rest_framework.response import Response
from .filters import UserBasicFilter
from django_filters.rest_framework import DjangoFilterBackend
logger = logging.getLogger(__name__)

parse_password_reset_data = partial(_parse_data, cls=PasswordResetSerializer)
class UserViewSet(viewsets.ModelViewSet):
 
    queryset = []
    permission_classes_by_action = {"data_manager": [accessPermission],
                                      "data_general":[accessPermission1],}

    serializer_class = UserSerializer

    @action(detail=False,methods=['GET']) 
    def data_manager(self,request):
        
        queryset=User.objects.all()

        Ser=UserSerializer(queryset,many=True)
        return Response(Ser.data)

    @action(detail=False,methods=['GET']) 
    def data_general(self,request):
        
        query=User.objects.filter(email=request.user.email)

        Ser=UserSerializer(query,many=True)
        return Response(Ser.data)


    @action(detail=False, methods=['POST'])
    def register(self, request):
        data = auth_register_user(request)
        return response.Created(data)

    @action(detail=False, methods=['POST'])
    def login(self, request):
        return auth_login(request)

        
    @action(detail=False, methods=['POST'])
    def logout(self, request):
        try:
            data = parse_password_reset_data(request.data)
            email = data.get('email')
            data=Token.objects.filter(key=email)
            data.delete()
            logout(request)
            return response.Ok({"detail": "Successfully logged out."})
        except:
            return response.Ok({"detail": "No User found"})
    
    @action(detail=False, methods=['POST'])
    def password_change(self, request):
        data = auth_password_change(request)
        user, new_password = data.get('email'), data.get('new_password')
        check=User.objects.get(email=user)
        if check.check_password(data.get('old_password')):
            check.set_password(new_password)
            check.save()
            content = {'success': 'Password changed successfully.'}
            return response.Ok(content)
        else:
            content = {'detail': 'Old password is incorrect.'}
            return response.BadRequest(content)

    @action(detail=False, methods=['POST'])
    def reset_mail(self, request):
        data = parse_password_reset_data(request.data)
        email = data.get('email')
        user, email_user, mobile_user = get_user_from_email_or_mobile(email)
        if not email_user:
            return response.BadRequest({'detail': 'User does not exists.'})
        if user:
            try:
                email = user.email
                password_reset_code = PasswordResetCode.objects.create_reset_code(user)
                password_reset_code.send_password_reset_email()
                message = "We have sent a password reset link to the {}. Use that link to set your new password".format(
                    email)
                return response.Ok({"detail": message})
            except get_user_model().DoesNotExist:
                message = "Email '{}' is not registered with us. Please provide a valid email id".format(email)
                message_dict = {'detail': message}
                return response.BadRequest(message_dict)
            except Exception:
                message = "Unable to send password reset link to email-id- {}".format(email)
                message_dict = {'detail': message}
                logger.exception(message)
                return response.BadRequest(message_dict)
        else:
            message = {'detail': 'User for this staff does not exist'}
            return response.BadRequest(message)

    @action(detail=False, methods=['POST'])
    def reset_password(self, request):
        code = request.data.get('code')
        password = request.data.get('password')
        if code:
            try:
                l1=list(code.split("/"))
                password_reset_code = PasswordResetCode.objects.get(code=l1[1])
                
                password_reset_code.user = get_user_model().objects.get(email=password_reset_code.user)
            except:
                message = 'Unable to verify user.'
                message_dict = {'detail': message}
                return response.BadRequest(message_dict)
        

            password_reset_code.user.set_password(password)
            password_reset_code.user.save()
            message = "Password Created successfully"
            message_dict = {'detail': message}
            return response.Ok({"success": message_dict})
        else:
            message = {'detail': 'Password reset link expired. Please re-generate password reset link. '}
            return response.BadRequest(message)

    @action(detail=False,methods=['GET'])
    def count(self,request):
        queryset=User.objects.all()
        self.filterset_class = UserBasicFilter
        queryset = UserBasicFilter.count_filter(self,queryset)

        return Response({'count':queryset})

    @action(detail=False,methods=['GET'])
    def count_by_gender(self,request):
        queryset=User.objects.all()
        self.filterset_class = UserBasicFilter
        queryset = UserBasicFilter.count_filter_gender(self,queryset)

        return Response({'count':queryset})


    def get_permissions(self):
        try:
            # return permission_classes depending on `action` 
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError: 
            # action is not set return default permission_classes
            return [permission() for permission in self.permission_classes]

   


