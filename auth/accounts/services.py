import random
import string
from collections import namedtuple
from functools import partial
from ..base import response
from django.contrib.auth import get_user_model
from django.contrib.auth import login
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import ValidationError

from .serializers import UserSerializer,UserRegistrationSerializer,LoginSerializer,PasswordChangeSerializer

User = namedtuple('User', ['email', 'mobile', 'password','role'])

def _parse_data(data, cls):

    serializer = cls(data=data)
    if not serializer.is_valid():
        raise ValidationError(serializer.errors)
    return serializer.validated_data


# Parse Auth login data
parse_register_user_data = partial(_parse_data, cls=UserRegistrationSerializer)
def auth_register_user(request):

    user_model = get_user_model()
    data = parse_register_user_data(request.data)
    user_data = User(
        mobile=data.get('mobile'),
        email=data.get('email'),
        password=data.get('password'),
        role=data.get('role'),
        gender=data.get('gender')
    )
    user = None

    try:
        user = get_user_model().objects.get(email=data.get('email'), is_active=True)
    except get_user_model().DoesNotExist:
        pass
    try:
        user = get_user_model().objects.get(mobile=data.get('mobile'), is_active=True)
    except get_user_model().DoesNotExist:
        pass
    
    if not user:
        un_active_user = user_model.objects.filter(email=user_data.email, is_active=False)
        if un_active_user:
            user_model.objects.filter(email=user_data.email, is_active=False).delete()

        user = user_model.objects.create_user(**dict(user_data._asdict()), is_active=True)
    user_obj = user_model.objects.filter(email=data.get('email')).first()
    if user_obj:
        user_obj.set_password(data.get('password'))
        user_obj.save()
    return UserRegistrationSerializer(user).data

parse_auth_login_data = partial(_parse_data, cls=LoginSerializer)
def auth_login(request):

    data, auth_data = parse_auth_login_data(request.data), None
    email, password = data.get('email'), data.get('password')
    if email and password:
        user, email_user, mobile_user = get_user_from_email_or_mobile(email)
        if not email_user and not mobile_user:
            return response.BadRequest({'detail': 'User does not exists.'})
        if user.check_password(password):
            if not user.is_active:
                return response.BadRequest({'detail': 'User account is disabled.'})
            auth_data = generate_auth_data(request, user)
            return response.Ok(auth_data)
        else:
            if mobile_user:
                return response.BadRequest({'detail': 'Incorrect Mobile Number and password.'})
            else:
                return response.BadRequest({'detail': 'Incorrect Email Id and password.'})
    else:
        return response.BadRequest({'detail': 'Must Include username and password.'})


def get_user_from_email_or_mobile(username):
    user_model = get_user_model()
    mobile_user = user_model.objects.filter(mobile=username).first()
    email_user = user_model.objects.filter(email=username).first()
    user = mobile_user if mobile_user else email_user
    return user, email_user, mobile_user


def generate_auth_data(request, user):
    token, created = Token.objects.get_or_create(user=user)
    login(request, user)
    auth_data = {
        "token": token.key,
        "user": UserSerializer(instance=user, context={'request': request}).data
    }
    return auth_data

parse_auth_password_change_data = partial(_parse_data, cls=PasswordChangeSerializer)

def auth_password_change(request):

    data = parse_auth_password_change_data(request.data)
    return data