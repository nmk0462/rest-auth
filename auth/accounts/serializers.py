from django.contrib.auth import get_user_model
from django.db import models
from rest_framework import serializers
from .models import User



class UserSerializer(serializers.ModelSerializer):
 

    class Meta:
        model = User
        fields = (
            'id', 'email','role','gender'
        
        )
        extra_kwargs = {'password': {'write_only': True}, 'last_login': {'read_only': True},
                        'is_superuser': {'read_only': True}}



class UserRegistrationSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        allow_blank=False,
        allow_null=False,
        error_messages={
            'required': 'Please enter a valid e-mail id.',
            'invalid': 'Please enter a valid e-mail id.',
            'blank': 'Please enter a valid e-mail id.',
            'null': 'Please enter a valid e-mail id.'
        },
    )

    class Meta:
        model = get_user_model()
        fields = (
            'id', 'email', 'password','mobile','role','gender'
            
        )
        extra_kwargs = {'password': {'write_only': True}}
        read_only_fields = ('id',)

    def validate_password(self, value):
        if len(value) > 7:
            return value
        else:
            msg = _('Password should have minimum 8 characters.')
            raise serializers.ValidationError(msg)


class LoginSerializer(serializers.Serializer):

    email = serializers.CharField(
        allow_blank=False,
        allow_null=False,
        error_messages={'required': 'Please enter a valid mobile/email id.',
                        'blank': 'Please enter a valid mobile/email id.',
                        'null': 'Please enter a valid mobile/email id.'}
    )
    password = serializers.CharField(max_length=128)


class PasswordChangeSerializer(serializers.Serializer):
    email=serializers.CharField(max_length=100)
    old_password = serializers.CharField(max_length=128)
    new_password = serializers.CharField(
        min_length=6,
        max_length=128,
        error_messages={'required': 'Please enter a valid password.',
                        'blank': 'Please enter a valid password.',
                        'null': 'Please enter a valid password.',
                        'min_length': 'Password should have minimum 6 characters.'}
    )

class PasswordResetSerializer(serializers.Serializer):
    email = serializers.CharField(
        allow_blank=False,
        allow_null=False,
        error_messages={'required': 'Please enter a valid email.',
                        'blank': 'Please enter a valid email.',
                        'null': 'Please enter a valid email'}
    )
